import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//
// a class to hold sizes
class Sizes {
  //
  // general padding
  static EdgeInsets getGeneralPadding(double blockSize) {
    return EdgeInsets.all((blockSize * 2));
  }

  //
  // rounded button padding
  static EdgeInsets getButtonPadding(double blockSize) {
    return EdgeInsets.symmetric(
      vertical: (blockSize * 2),
      horizontal: (blockSize * 4),
    );
  }

  //
  // counter container padding
  static EdgeInsets getCounterContainerPadding(double blockSize) {
    return EdgeInsets.only(
      top: blockSize * 4,
      left: (blockSize * 4),
      right: (blockSize * 4),
    );
  }

  //
  // general border radius
  static BorderRadius getGeneralBorderRadius(double blockSize) {
    return BorderRadius.circular((blockSize * 2));
  }

  //
  // text field size
  static double getTextFieldSize(double blockSize) {
    return (blockSize * 40);
  }

  //
  // circular progress sizes
  static double getCircularProgressSizes(double blocksize) {
    return (blocksize * 4);
  }

  //
  // task title font size
  static double taskTitleFontSize(double blockSize) {
    return (blockSize * 3);
  }

  //
  // task time font size
  static double timeFontSize(double blockSize) {
    return (blockSize * 1.5);
  }

  //
  // contacts font size
  static double contactsFontSize(double blockSize) {
    return (blockSize * 4);
  }
}
