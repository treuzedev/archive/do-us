//
// a class to hold strings
class Strings {
  //
  // format message date to display
  static String formatDate(String date) {
    //
    // extract day and time
    String day = RegExp(r'^[^\s]+').stringMatch(date);
    String time = RegExp(r'[^\s]+$').stringMatch(date);

    //
    // format time
    time = RegExp(r'\d*.\d*').stringMatch(time);

    //
    // format date
    List<String> tmp = day.split('-');
    tmp[0] = RegExp(r'\d{2}$').stringMatch(tmp[0]);
    day = tmp.reversed.join('-');

    //
    // return
    return time + '\n' + day;
  }

  //
  // string constants
  static const String appTitle = 'Do Us!';
  static const String nullLabel = 'null';
  static const String homeText = 'Completed Do Us: ';
  static const String loadingLabel = 'Loading..';
  static const String newTaskLabel = 'New Do Us';
  static const String addLabel = 'Add';
  static const String clearEntriesLabel = 'Clear All Do Us';
  static const String resetCounterLabel = 'Reset Do Us';
  static const String aboutLabel = 'About';
  static const String contactsLabel = 'Contacts';
  static const String deletedLabel = 'Do Us Deleted!';
  static const String homeLabel = 'Home';
  static const String aboutText = 'Hello there!\n\n'
      'Use the Home screen bottom button to make a text field appear, where you can add a new Do Us.\n\n'
      'After they show up on screen, you can tap the checkbox to mark them as completed / uncompleted, edit the title by double tapping a Do Us or delete a Do Us if you long press one of them.\n\n'
      'Still in the Home Screen, you can long press the container for the completed Do Us, if you wish to reset the count, or, if you click the delete button on the top right corner, you can choose to delete all of the current Do Us on screen.\n\n'
      'Finally, in the top left corner, you have a drawer menu available to navigate to the rest of the app.\n\n'
      'Go forth and Do Us!';
  static const String contactsText =
      'Got questions?\nSuggestions?\nAnything to say?\n\n'
      'Hit me up at:\n\n'
      'https://github.com/eduardoaemunoz\n\n'
      'or\n\n'
      'eduardoaemunoz\n@gmail.com';
  static const String yesLabel = 'Yes';
  static const String noLabel = 'No';
  static const String deleteDialog =
      'Are you sure you want to delete all Do Us?';
  static const String resetDialog =
      'Are you sure you want reset the Do Us count?';
  static const String taskViewLabel = 'Add Some Do Us!';
}
