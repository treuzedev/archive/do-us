import 'package:dome/classes/colors.dart';
import 'package:dome/classes/shared_view_model.dart';
import 'package:dome/classes/strings.dart';
import 'package:dome/widgets/modalBottomSheetContent.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

//
// what shows up to add a task
class HomeBottomSheet extends StatelessWidget {
  //
  // constructor
  HomeBottomSheet(
      {@required this.blockSize,
      @required this.buttonOnPressed,
      @required this.onTextChanged});

  final double blockSize;
  final Function buttonOnPressed;
  final Function onTextChanged;

  //
  // return widget
  @override
  Widget build(BuildContext context) {
    return Consumer<SharedViewModel>(
      builder: (context, sharedViewModel, child) {
        //
        // return widget
        return ModalBottomSheetContent(
          blockSize: blockSize,
          backgroundColor: MyColors.mediumLightColor,
          buttonColor: MyColors.mediumDarkColor,
          text: Strings.addLabel,
          onTextChange: onTextChanged,
          buttonOnPressed: buttonOnPressed,
        );
      },
    );
  }
}
