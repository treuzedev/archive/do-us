import 'package:dome/classes/colors.dart';
import 'package:dome/classes/sizes.dart';
import 'package:dome/widgets/round_material_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

//
// customize bottom sheet content
class ModalBottomSheetContent extends StatelessWidget {
  //
  // constructor
  ModalBottomSheetContent(
      {@required this.blockSize,
      @required this.text,
      @required this.backgroundColor,
      @required this.buttonColor,
      @required this.buttonOnPressed,
      @required this.onTextChange});

  final double blockSize;
  final Color backgroundColor;
  final Color buttonColor;
  final Function buttonOnPressed;
  final Function onTextChange;
  final String text;

  //
  // build method
  // in order to make the content fit correctly, use a single child scroll view
  // to put the container on top of the keyboard, add necessary padding
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Container(
          color: MyColors.bottomSheetTransparentLikeColor,
          child: Container(
            decoration: BoxDecoration(
              color: backgroundColor,
              borderRadius: Sizes.getGeneralBorderRadius(blockSize),
            ),
            child: Padding(
              padding: Sizes.getGeneralPadding(blockSize),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: Sizes.getGeneralPadding(blockSize),
                    child: TextField(
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      decoration: InputDecoration(
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: MyColors.mediumDarkColor,
                          ),
                        ),
                      ),
                      cursorColor: MyColors.mediumDarkColor,
                      maxLines: null,
                      textAlign: TextAlign.center,
                      onChanged: onTextChange,
                      autofocus: true,
                    ),
                  ),
                  RoundMaterialButton(
                    blockSize: blockSize,
                    text: text,
                    color: buttonColor,
                    onPressed: buttonOnPressed,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
